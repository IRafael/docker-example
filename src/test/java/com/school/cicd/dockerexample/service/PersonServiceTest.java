package com.school.cicd.dockerexample.service;

import com.school.cicd.dockerexample.entity.Person;
import com.school.cicd.dockerexample.repository.PersonRepository;
import org.junit.jupiter.api.Test;

class PersonServiceTest {

    private final PersonRepository personRepository = new PersonRepository(5);
    private final PersonService personService = new PersonService(personRepository);

    @Test
    void getPerson() {
        final Person person = personService.getPerson("1");
        assert person.getId().equals("1");
    }
}