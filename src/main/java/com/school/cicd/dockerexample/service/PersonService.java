package com.school.cicd.dockerexample.service;

import com.school.cicd.dockerexample.entity.Person;
import com.school.cicd.dockerexample.repository.PersonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PersonService {

    private final PersonRepository personRepository;

    public Person getPerson(final String id) {
        final Optional<Person> byId = personRepository.findById(id);
        return byId.orElse(new Person(
                id,
                "N/A",
                "N/A",
                "N/A"
        ));
    }

    public List<Person> getAllPersons() {
        return personRepository.findAll();
    }

    public List<Person> getAllPersons(int limit) {
        return personRepository.findN(limit);
    }

    public Person addPerson(final String name, final String surname, final String tag) {
        return personRepository.addPerson(name, surname, tag);
    }
}
