package com.school.cicd.dockerexample.controller;

import com.school.cicd.dockerexample.controller.request.AddPersonRequest;
import com.school.cicd.dockerexample.entity.Person;
import com.school.cicd.dockerexample.service.PersonService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/person")
@RequiredArgsConstructor
public class PersonController {

    private final PersonService personService;

    @GetMapping(path = "/{id}")
    public ResponseEntity<Person> getPersonById(@PathVariable final String id) {
        return ResponseEntity.ok(personService.getPerson(id));
    }

    @GetMapping
    public ResponseEntity<List<Person>> getAllPersons(@RequestParam(name = "limit", required = false) final Integer limit) {
        if (limit == null) {
            return ResponseEntity.ok(personService.getAllPersons());
        }
        return ResponseEntity.ok(personService.getAllPersons(limit.intValue()));

    }

    @PostMapping
    public ResponseEntity<Person> addPerson(@RequestBody final AddPersonRequest request) {
        return ResponseEntity.ok(
                personService.addPerson(
                        request.getName(),
                        request.getSurname(),
                        request.getTag()
                )
        );
    }

}
