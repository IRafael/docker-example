package com.school.cicd.dockerexample.controller.request;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class AddPersonRequest {

    private final String name;
    private final String surname;
    private final String tag;

}
