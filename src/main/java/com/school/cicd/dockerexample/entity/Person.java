package com.school.cicd.dockerexample.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Person {

    private final String id;
    private final String name;
    private final String surname;
    private final String tag;

}

