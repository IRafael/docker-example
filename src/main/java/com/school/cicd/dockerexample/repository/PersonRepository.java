package com.school.cicd.dockerexample.repository;

import com.school.cicd.dockerexample.entity.Person;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.IntStream;

@Repository
public class PersonRepository {
    private final Map<String, Person> persons = new HashMap<>();
    private final String[] names = new String[]{"Ivan", "Daniil", "Rafael"};
    private final String[] surnames = new String[]{"Ivanov", "Daniilov", "Iakupov"};
    private final String[] tags = new String[]{"teacher", "manager", "architect"};
    private final PrimitiveIterator.OfInt iterator = IntStream.range(1, Integer.MAX_VALUE).iterator();

    private final int personsAmount;

    public PersonRepository(@Value("${personsAmount:100}") final int personsAmount) {
        this.personsAmount = personsAmount;
    }

    @PostConstruct
    private void init() {
        IntStream.range(1, this.personsAmount + 1 ).forEach(id -> {
            final Person person = new Person(
                    String.valueOf(iterator.nextInt()),
                    getRandomElement(names),
                    getRandomElement(surnames),
                    getRandomElement(tags)
            );
            persons.put(person.getId(), person);
        });
    }

    private static String getRandomElement(String[] array) {
        final Random rand = new Random();

        int index = rand.nextInt(array.length);

        return array[index];
    }

    public Optional<Person> findById(final String id) {
        if (persons.containsKey(id)) {
            return Optional.of(persons.get(id));
        }
        return Optional.empty();
    }

    public List<Person> findAll() {
        return persons.values().stream().toList();
    }

    public List<Person> findN(int limit) {
        return persons.values().stream().limit(limit).toList();
    }

    public Person addPerson(final String name, final String surname, final String tag) {
        final Person person = new Person(
                String.valueOf(iterator.nextInt()),
                name,
                surname,
                tag
        );
        persons.put(person.getId(), person);
        return person;
    }
}
