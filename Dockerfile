FROM eclipse-temurin:19
COPY ./target/docker-example-0.0.1-SNAPSHOT.jar ./app/app.jar
ENTRYPOINT ["java", "-jar", "./app/app.jar"]
